module.exports = {
  env: {
    node: true,
    es6: true
  },
  extends: 'standard',
  parserOptions: {
    parser: 'babel-eslint'
  },
  extends: [
    // https://github.com/vuejs/eslint-plugin-vue#priority-a-essential-error-prevention
    'plugin:vue/recommended',
    'standard'
  ],
  rules: {
    // Disable some Vue rules
    'vue/max-attributes-per-line': 'off',
    'vue/singleline-html-element-content-newline': 'off',
    'vue/component-name-in-template-casing': [ 'error', 'kebab-case', { 'ignores': [] } ],
    // allow debug during development
    // 'no-console': process.env.NODE_ENV !== 'production' ? 'off' : 'error',
    'no-debugger': process.env.NODE_ENV !== 'production' ? 'off' : 'error'
  }
}
