---
layout: AboutLayout
title: A propos

skills_title: Compétences
skills: [
  { title: 'VueJs', level: 80 }
]

formations_title: Formations
formations: [
  { title: 'ENSSAT Lannion', description: 'Logiciel et Système Informatique', year: 2007 }
]

interests_title: Loisirs
interests: [
  'Jeux vidéo'
]
---
Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
