---
home: true
layout: HomeLayout
title: Sylvain Favé
tagline: Développeur Front-End à Rennes
profiles: [
  { network: "GitLab", icon: "icon-gitlab", url: "https://gitlab.com/Finrod/" },
  { network: "Stack Overflow", icon: "icon-stack-overflow", url: "https://stackoverflow.com/users/3493954/" },
  { network: "Linkedin", icon: "icon-linkedin", url: "https://www.linkedin.com/in/sfave/" },
  { network: "Twitter", icon: "icon-twitter", url: "https://twitter.com/Finrod" },
  { network: "Doyoubuzz", icon: "icon-user", url: "https://www.doyoubuzz.com/sylvain-fave" }
]
---
