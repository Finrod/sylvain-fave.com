---
layout: WorksLayout
title: Projets
personnal: [{
  title: "Team Bac à Sable",
  description: "Application pour mon groupe d'entrainement.",
  url: "https://tbas.finrod.info",
  repo: "https://gitlab.com/Finrod/",
  tags: ['vuejs', 'firebase', 'PWA']
}]
professional: [{
  title: "Wanesy Management Center",
  customer: "Kerlink (prestation Créative Ingénierie)",
  description: "Application d'administration et de gestion d'un réseau IOT.",
  url: "",
  repo: "",
  tags: ['vuejs', 'IOT']
}, {
  title: "MAC2",
  customer: "PSA (prestation Néo-Soft)",
  description: "Application d'administration et de création de dashboard de surveillance de lignes de production.",
  url: "",
  repo: "",
  tags: ['angularJS']
}, {
  title: "Blah Blah",
  customer: "Blah",
  description: "Blah blah blah.",
  url: "",
  repo: "",
  tags: ['blah', 'blah']
}]
---
