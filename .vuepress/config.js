module.exports = {
  // Site description
  title: 'Sylvain Favé - Développeur front-end à Rennes',
  description: 'Sylvain Favé - Développeur front-end à Rennes',

  // Theme configuration
  head: [
    ['link', { rel: 'icon', href: '/favicon.png' }]
  ],
  themeConfig: {
    nav: [
      // A propos
      // - un paragraphe de description: d'où je viens, ce que j'ai fait, ce que je fais...
      // - un bloc "Compétences"
      // - un bloc "Formations"
      // - un bloc "Loisirs"

      // Projets
      // - Une phrase d'introduction
      // - Une liste des projets personnels et professionels
      //   - une image d'illustration si possible
      //   - un titre
      //   - une liste de tags
      //   - une phrase de descriptions du contexte
      //   - une liste des fonctionnalités techniques / métiers implémentées

      // Contact ?
      // - lien vers le formulaire de contact de mon blog ?
      // - lien vers les réseaux sociaux pour me contacter ?
      // - affichage de mes coordonnées ?
      // - > un bloc contact dans la page "A propos" ?

      // { text: 'A propos', link: '/about/' },
      // { text: 'Projets', link: '/works/' },
      // { text: 'Contact', link: '/contact/' }
    ]
  },

  // Build configuration
  evergreen: true
}
